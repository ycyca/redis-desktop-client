import net from 'net'
import Redis from 'ioredis'
const Client = require('ssh2').Client

export function connectRedis (config, resolve, errResolve) {
  if (config.ssh) {
    const conn = new Client()
    conn.on('ready', () => {
      console.log('Client :: ready')
      const server = net.createServer(function (sock) {
        conn.forwardOut(sock.remoteAddress, sock.remotePort, config.host, config.port, (err, stream) => {
          if (err) {
            sock.end()
          } else {
            sock.pipe(stream).pipe(sock)
          }
        })
      }).listen(0, function () {
        createRedisClient(config, resolve, { host: '127.0.0.1', port: server.address().port })
      })
    }).on('error', err => {
      errResolve(err)
    })
    try {
      const sshConfig = {
        host: config.sshHost,
        port: config.sshPort,
        username: config.sshUser
      }
      if (config.sshKey) {
        conn.connect(Object.assign(sshConfig, {
          privateKey: config.sshKey,
          passphrase: config.sshKeyPassphrase
        }))
      } else {
        conn.connect(Object.assign(sshConfig, {
          password: config.sshPassword
        }))
      }
    } catch (err) {
      errResolve(err)
    }
  } else {
    createRedisClient(config, resolve)
  }
}

function createRedisClient (config, resolve, override) {
  if (config.ssl) {
    config.tls = {
      rejectUnauthorized: false
    }
    if (config.tlsca) config.tls.ca = config.tlsca
    if (config.tlskey) config.tls.key = config.tlskey
    if (config.tlscert) config.tls.cert = config.tlscert
  }
  const redis = new Redis(Object.assign({}, config, override, {
    retryStrategy () {
      return false
    }
  }))
  resolve(redis)
}
